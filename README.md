# API DOC
Made by Pyralink team
# Topic

#User
1. [Register](#markdown-header-register)
2. [Verify Register](#markdown-header-verify-register)
3. [Resend Token Verify Sign up](#markdown-header-resend-token-verify-sign-up)
4. [Sign in](#markdown-header-sign-in)
5. [Token](#markdown-header-token)
6. [Forgot password](#markdown-header-forgot-pasword)
7. [Verify forgot password](#markdown-header-verify-forgot-password)
8. [Change password](#markdown-header-change-password)

# IoT 
1. [Connect Policy](#markdown-header-connect-policy)
2. [Publish Policy](#markdown-header-publish-policy)
3. [Subcribe Policy](#markdown-header-subcribe-policy)
4. [Recieve Policy](#markdown-header-recieve-policy)
5. [Topic IoT](#markdown-header-topic-iot)


##Register New Account
`POST https://api.pyralink.io/v1/auth/signup`
### Params
````
{
    "phone_number" : STRING,
    "user_name"    : STRING
}
````
###Sample request:
```
{
    "phone_number": "+841698785471",
    "password"    : "123abcABC@"
    "username": "Bảo Nguyên"
}
```
### Required
- Kiểm tra đầy đủ thông tin người dùng trước khi gửi lên
- Thêm mã quốc gia vào trước số điện thoại người dùng
- Mật khẩu người dùng phải từ 6 ký tự trở lên
- Kết nối thông qua https
### Code
- 2510: Đăng ký thành công, qua bước xác thực.
- 5555: Lỗi Server.
- 6666: Các thông tin gửi lên chưa đầy đủ. 
- 7777: Lỗi không biết, có thông báo.
- 2601: Mật khẩu không đủ mạnh.
- 2602: Tài khoản đã tồn tại.
###Sample Respone
```
{
    "status" : "success",
    "code"   : 2510 
}
```
```
{
    "status" : "err",
    "code"   : 2604 
}
```
##Verify Register
[Top](#markdown-header-topic)

`POST https://api.pyralink.io/v1/auth/signup/verify`
### Params
````
{
    "phone_number" : STRING,
    "ConfirmationCode"    : STRING
}
````
###Sample request:
```
{
    "phone_number": "+841698785471",
    "ConfirmationCode": "12345678"
}
```
###Require
- Kiểm tra đầy đủ thông tin trước khi gửi lên
- Thêm mã quốc gia vào trước số điện thoại người dùng
###Code
- 2510: Xác thực thành công.
- 5555: Lỗi Server.
- 6666: Các thông tin gửi lên chưa đầy đủ. 
- 7777: Lỗi không biết, có thông báo.
- 2701: Mã code không đúng.
- 2702: Tài khoản chưa được đăng ký
- 2703: Tài khoản đã được xác thực
###Sample Respone
```
{
    "status" : "success",
    "code"   : 2510,
}
```
```
{
    "status" : "err",
    "code"   : 2701 
}
```
##Resend Token Verify Sign up
[Top](#markdown-header-topic)

`POST https://api.pyralink.io/v1/auth/signup/resendtoken`
### Params
````
{
    "phone_number" : STRING
}
````
###Sample request:
```
{
    "phone_number": "+841698785471"
}
```
###Code
- 2510: Xác thực thành công.
- 5555: Lỗi Server.
- 6666: Các thông tin gửi lên chưa đầy đủ. 
- 7777: Lỗi không biết, có thông báo.
- 2801: Tài khoản không tồn tại.
- 2802: Tài khoản đã được xác thực.
###Sample Respone
```
{
    "status" : "success",
    "code"   : 2510,
}
```

##Sign in
[Top](#markdown-header-topic)

`POST https://api.pyralink.io/v1/auth/signin`

### Params
````
{
    "phone_number" : String,
    "password" : String
}
````
###Sample request:

```
{
    "username" : "+841698785471",
    "password" : "12345678"
}
```
###Required
- Kiểm tra đầy đủ thông tin người dùng trước khi gửi lên
- Thêm mã quốc gia vào trước số điện thoại người dùng
###Code
- 2510: Đăng nhập thành công.
- 5555: Lỗi Server.
- 6666: Các thông tin gửi lên chưa đầy đủ. 
- 7777: Lỗi không biết, có thông báo.
- 2901: Sai tài khoản hoặc mật khẩu.
- 2902: Tài khoản không tồn tại.
###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
    "data"  :
    {
        "token" : 
        {
            "accessToken": String,
            "idToken":    String,
            "refreshToken": String
        },
        "sub"  : String
    } 
}
```

```
{
    "status" : "err",
    "code"   :  2702 
}
```

##Token
[Top](#markdown-header-topic)

```
{
    accessToken: 'String here',
    idToken: 'String here',
    refreshToken: 'String here'
}
```

###Note
- Sử dụng refreshToken, idToken để thực hiện các request đến Server (Authen, Get data,...)
- Sử dụng accessToken để thao tác với IoT Core (connect, sub, pub,...)
- Token được thêm vào http header khi request với ```Bearer Token``` để kiểm tra trạng thái đăng nhập ...

###Sample header
```
key: Authorization
value: Bearer idotkenhere.123456789.xxx
```

##Forgot password
[Top](#markdown-header-topic)

`POST https://api.pyralink.io/v1/auth/forgotpassword`

### Params
````
{
    "phone_number" : String
}
````
###Sample request:

```
{
    "phone_number" : "+841698785471"
}
```

###Code
- 2510: Thành công, qua bước xác thực
- 5555: Lỗi Server.
- 6666: Các thông tin gửi lên chưa đầy đủ. 
- 7777: Lỗi không biết, có thông báo.
- 8888: Bị hạn chế yêu cầu gửi tin nhắn.
- 3000: Tài khoản không tồn tại.
###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
}
```

##Verify forgot password
[Top](#markdown-header-topic)

`POST https://api.pyralink.io/v1/auth/forgotpassword/verify`

### Params
````
{
    "phone_number" : String,
    "ConfirmationCode": String
}
````

###Code
- 2510: Thành công, hãy đăng nhập lại
- 5555: Lỗi Server.
- 6666: Các thông tin gửi lên chưa đầy đủ. 
- 7777: Lỗi không biết, có thông báo.
- 3101: Mã code không đúng.
- 3202: Tài khoản không tồn tại.
- 3303: Tài khoản chưa được xác thực.
###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
}
```
##Change password
[Top](#markdown-header-topic)

`POST https://api.pyralink.io/v1/auth/changepassword`

#IoT
[Top](#markdown-header-topic)
````
host: a1tqxkj7szwc5k.iot.ap-southeast-1.amazonaws.com
region: ap-southeast-1
protocol: wss
````

## Connect Policy
[Top](#markdown-header-topic)
`POST https://api.pyralink.io/v1/policy/attach_connect`

###Code
- 2510: Thành công, hãy đăng nhập lại
- 5555: Lỗi Server.
- 7777: Lỗi không biết, có thông báo.

###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
}
```

## Publish Policy
[Top](#markdown-header-topic)
`POST https://api.pyralink.io/v1/policy/attach_publish`

###Code
- 2510: Thành công, hãy đăng nhập lại
- 5555: Lỗi Server.
- 7777: Lỗi không biết, có thông báo.

###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
}
```

## Subscibe Policy
[Top](#markdown-header-topic)
`POST https://api.pyralink.io/v1/policy/attach_subscribe`

###Code
- 2510: Thành công, hãy đăng nhập lại
- 5555: Lỗi Server.
- 7777: Lỗi không biết, có thông báo.

###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
}
```

## Recieve Policy

`POST https://api.pyralink.io/v1/policy/attach_receive`

###Code
- 2510: Thành công, hãy đăng nhập lại
- 5555: Lỗi Server.
- 7777: Lỗi không biết, có thông báo.

###Sample Respone

```
{
    "status" : "success",
    "code"   : 2510
}
```

## Topic


